package com.example.gladyu.protect_eyes;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by gladyu on 16/2/23.
 */
public class SeekCircleView extends View {

    private int pointX;
    private int pointY;
    private Paint percentPaint;
    private int percent;
    private int lineColorHight;
    private int lineColorLow;

    private int allLineWidth;
    private int percentLineWidth;
    private int lineHeight;
    private float angle;

    public SeekCircleView(Context context) {
       this(context,null);

    }

    public SeekCircleView(Context context, AttributeSet attrs) {
        this(context,attrs,0);
    }

    public SeekCircleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // TODO Auto-generated method stub
//        Resources resources = getResources();
//        percent = resources.getInteger(R.integer.percent);
//        allLineWidth = resources.getInteger(R.integer.allLineWidth);
//        percentLineWidth =resources.getInteger(R.integer.percentLineWidth);
//        lineHeight = resources.getInteger(R.integer.LineHeight);
//        angle =resources.getInteger(R.integer.angle);
//        allLineColor = resources.getColor(R.color.allLineColor);
//        percentLineColor =resources.getColor(R.color.percentColor);
        final TypedArray array = getContext().obtainStyledAttributes(
                attrs, R.styleable.SeekCircleView, defStyle, 0);
        percent = array.getInt(R.styleable.SeekCircleView_percent, 0);
        lineColorLow = array.getColor(R.styleable.SeekCircleView_LineColorLow, Color.GRAY);
        lineColorHight = array.getColor(R.styleable.SeekCircleView_LineColorHight, Color.GREEN);
        angle = array.getInt(R.styleable.SeekCircleView_angle, 270);
        lineHeight = array.getInt(R.styleable.SeekCircleView_LineHeight, 40);
        allLineWidth = array.getInt(R.styleable.SeekCircleView_allLineWidth, 4);
        percentLineWidth =array.getInt(R.styleable.SeekCircleView_percentLineWidth,8);
        percentPaint = new Paint();
        percentPaint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);

        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        pointX =  width/2;
        pointY = height/2;

        percentPaint.setColor(lineColorLow);
        percentPaint.setStrokeWidth(allLineWidth);

        float degrees = angle/100;

        canvas.save();
        canvas.translate(0, pointY);
        canvas.rotate((360-angle)/2 - 90, pointX, 0);
        for(int i = 0;i<100;i++){
            canvas.drawLine(0, 0, lineHeight, 0, percentPaint);
            canvas.rotate(degrees, pointX, 0);
        }
        canvas.restore();

        percentPaint.setStrokeWidth(percentLineWidth);
        percentPaint.setColor(lineColorHight);
        canvas.save();
        canvas.translate(0,pointY);
        canvas.rotate((360-angle)/2 - 90, pointX, 0);
        for(int i = 0;i<percent;i++){
            if(i == percent -1){
                percentPaint.setStrokeWidth(percentLineWidth*3);
            }
            canvas.drawLine(0, 0, lineHeight, 0, percentPaint);
            canvas.rotate(degrees, pointX, 0);
        }
        canvas.restore();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // TODO Auto-generated method stub
        //super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int d = (width >= height) ? height : width;
        setMeasuredDimension(d,d);
    }

    public void setPercent(int percent) {
        // TODO Auto-generated method stub
        this.percent = percent;
        postInvalidate();
    }
    public int getPointX() {
        return pointX;
    }

    public int getPointY() {
        return pointY;
    }

    public float getAngle() {
        return angle;
    }
}