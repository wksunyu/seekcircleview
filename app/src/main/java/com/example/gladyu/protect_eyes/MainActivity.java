package com.example.gladyu.protect_eyes;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity implements View.OnTouchListener, View.OnClickListener, AdapterView.OnItemClickListener {
    private Boolean updateSwitch;
    private SeekCircleView seekCircleView;
    private Button open;
    TextView percentText;
    RelativeLayout relativeLayout;
    private float scale;
    private float angle;
    private SharedPreferences sp;
    private float relativeProgress;
    private ListView listView;
    private String[] colors = new String[]{"黄色", "绿色", "蓝色"};
    private String[] infos = new String[]{"使用", "使用好", "各种实用"};
    private String[] use_infos = new String[]{"吸收黄光 好牛逼啊", "吸收绿光 好牛逼啊", "吸收蓝种光 好牛逼啊"};
    private int res_Color;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        seekCircleView.setOnTouchListener(this);
        open.setOnClickListener(this);
        listView.setOnItemClickListener(this);
    }

    public void init() {
        open = (Button) findViewById(R.id.open);
        seekCircleView = (SeekCircleView) findViewById(R.id.drawView);
        percentText = (TextView) findViewById(R.id.percent);
        listView = (ListView) findViewById(R.id.listView);
        listView.setVerticalScrollBarEnabled(false);
        relativeLayout = (RelativeLayout) findViewById(R.id.layout);

        SimpleAdapter simpleAdapter = new SimpleAdapter(this, getDate(), R.layout.list_layout, new String[]{"image", "color", "info", "use_info"},
                new int[]{R.id.image, R.id.color, R.id.info, R.id.use_info});
        listView.setAdapter(simpleAdapter);
        angle = seekCircleView.getAngle();
        seekCircleView.invalidate();
        scale = angle / 360;// 显示的圆弧占完整圆的比例
        sp = getSharedPreferences("Switch", MODE_PRIVATE);
        updateSwitch = sp.getBoolean("updateSwitch", true);
        relativeProgress = sp.getFloat("progress", 0);
        res_Color = sp.getInt("color", Color.GRAY);
        if (relativeProgress > 100)
            relativeProgress = 100;
        if (relativeProgress < 0)
            relativeProgress = 0;
        saveScreenBrightness(MainActivity.this,(int)relativeProgress);
        setScreenBrightness(MainActivity.this,relativeProgress);
        seekCircleView.setPercent((int) relativeProgress);
        percentText.setText((int) relativeProgress + "%");
        relativeLayout.setBackgroundColor(res_Color);
        open.setText(updateSwitch != true ? "开启护眼模式" : "关闭护眼模式");
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float x = event.getX() - seekCircleView.getPointX();
        float y = seekCircleView.getPointY() - event.getY();
        float position = (float) ((Math.atan2(x, y) + scale * Math.PI) / (Math.PI * 2 * scale));
        relativeProgress = Math.round(position * 100);
        Log.i("position", position + "");
        Log.i("relative", relativeProgress + "");
        if (relativeProgress >= 0 && relativeProgress <= 100 && updateSwitch) {
            seekCircleView.setPercent((int) relativeProgress);
            percentText.setText((int) relativeProgress + "%");
            saveScreenBrightness(MainActivity.this,(int)relativeProgress);
            setScreenBrightness(MainActivity.this,relativeProgress);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        open.setText(updateSwitch == true ? "开启护眼模式" : "关闭护眼模式");
        updateSwitch = updateSwitch != true;
        Log.i("updateSwitch", updateSwitch + "");
    }

    @Override
    protected void onPause() {
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean("updateSwitch", updateSwitch);
        editor.putFloat("progress", (int) relativeProgress);
        editor.putInt("color", res_Color);
        editor.commit();
        super.onPause();
    }

    private List<Map<String, Object>> getDate() {
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        for (int i = 0; i < colors.length; i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("color", colors[i]);
            map.put("image", R.drawable.picture);
            map.put("info", infos[i]);
            map.put("use_info", use_infos[i]);
            list.add(map);
        }
        return list;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                relativeLayout.setBackgroundColor(Color.YELLOW);
                res_Color = Color.YELLOW;
                break;
            case 1:
                relativeLayout.setBackgroundColor(Color.GREEN);
                res_Color = Color.GREEN;
                break;
            case 2:
                relativeLayout.setBackgroundColor(Color.BLUE);
                res_Color = Color.BLUE;
                break;
        }

    }
    public static void setScreenBrightness(Activity activity,float paramInt){
        Window localWindow = activity.getWindow();
        WindowManager.LayoutParams params = localWindow.getAttributes();
        params.screenBrightness = paramInt* 255/100;
        localWindow.setAttributes(params);

    }
    private static void saveScreenBrightness(Context context, int paramInt){
        ContentResolver resolver = context.getContentResolver();
        Uri uri = Settings.System.getUriFor(Settings.System.SCREEN_BRIGHTNESS);
        Settings.System.putInt(resolver, Settings.System.SCREEN_BRIGHTNESS, paramInt);
        resolver.notifyChange(uri, null);
    }
}
